/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.github.angads25.filepicker;

public final class R {
    private R() {}

    public static final class anim {
        private anim() {}

        public static final int marked_item_animation = 0x7f01000a;
        public static final int unmarked_item_animation = 0x7f01000d;
    }
    public static final class attr {
        private attr() {}

        public static final int error_dir = 0x7f02006e;
        public static final int extensions = 0x7f020070;
        public static final int offset_dir = 0x7f0200a1;
        public static final int root_dir = 0x7f0200b6;
        public static final int selection_mode = 0x7f0200bd;
        public static final int selection_type = 0x7f0200be;
        public static final int title_text = 0x7f0200f4;
    }
    public static final class color {
        private color() {}

        public static final int colorAccent = 0x7f040027;
        public static final int colorHeader = 0x7f040028;
        public static final int colorPrimary = 0x7f040029;
        public static final int colorPrimaryDark = 0x7f04002a;
        public static final int textColorPrimary = 0x7f040057;
        public static final int textColorSecondary = 0x7f040058;
    }
    public static final class dimen {
        private dimen() {}

        public static final int activity_margin = 0x7f05004a;
        public static final int checkbox_dimens = 0x7f05004b;
        public static final int checkbox_margin = 0x7f05004c;
        public static final int toolbar_image_margin = 0x7f05006a;
    }
    public static final class drawable {
        private drawable() {}

        public static final int bottom_shadow = 0x7f060054;
    }
    public static final class id {
        private id() {}

        public static final int cancel = 0x7f070022;
        public static final int dir_path = 0x7f07002c;
        public static final int dir_select = 0x7f07002d;
        public static final int dname = 0x7f07002f;
        public static final int fileList = 0x7f070035;
        public static final int file_dir_select = 0x7f070036;
        public static final int file_mark = 0x7f070037;
        public static final int file_select = 0x7f070038;
        public static final int fname = 0x7f070039;
        public static final int footer = 0x7f07003a;
        public static final int ftype = 0x7f07003c;
        public static final int header = 0x7f07003d;
        public static final int imageView = 0x7f070044;
        public static final int image_type = 0x7f070045;
        public static final int linearLayout = 0x7f07004a;
        public static final int multi_mode = 0x7f070050;
        public static final int select = 0x7f07006d;
        public static final int single_mode = 0x7f070073;
        public static final int title = 0x7f070082;
    }
    public static final class layout {
        private layout() {}

        public static final int dialog_file_list = 0x7f09001c;
        public static final int dialog_file_list_item = 0x7f09001d;
        public static final int dialog_footer = 0x7f09001e;
        public static final int dialog_header = 0x7f09001f;
        public static final int dialog_main = 0x7f090020;
    }
    public static final class mipmap {
        private mipmap() {}

        public static final int ic_directory_parent = 0x7f0a0000;
        public static final int ic_type_file = 0x7f0a0003;
        public static final int ic_type_folder = 0x7f0a0004;
    }
    public static final class string {
        private string() {}

        public static final int cancel_button_label = 0x7f0b0020;
        public static final int choose_button_label = 0x7f0b0021;
        public static final int default_dir = 0x7f0b0022;
        public static final int error_dir_access = 0x7f0b0025;
        public static final int label_parent_dir = 0x7f0b0026;
        public static final int label_parent_directory = 0x7f0b0027;
        public static final int last_edit = 0x7f0b0028;
    }
    public static final class styleable {
        private styleable() {}

        public static final int[] FilePickerPreference = { 0x7f02006e, 0x7f020070, 0x7f0200a1, 0x7f0200b6, 0x7f0200bd, 0x7f0200be, 0x7f0200f4 };
        public static final int FilePickerPreference_error_dir = 0;
        public static final int FilePickerPreference_extensions = 1;
        public static final int FilePickerPreference_offset_dir = 2;
        public static final int FilePickerPreference_root_dir = 3;
        public static final int FilePickerPreference_selection_mode = 4;
        public static final int FilePickerPreference_selection_type = 5;
        public static final int FilePickerPreference_title_text = 6;
    }
}
