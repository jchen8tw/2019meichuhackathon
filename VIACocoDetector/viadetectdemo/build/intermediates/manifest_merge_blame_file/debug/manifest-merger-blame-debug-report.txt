1<?xml version="1.0" encoding="utf-8"?>
2<manifest xmlns:android="http://schemas.android.com/apk/res/android"
3    package="com.via.viadetectdemo"
4    android:versionCode="1"
5    android:versionName="1.0" >
6
7    <uses-sdk
8        android:minSdkVersion="26"
8-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml
9        android:targetSdkVersion="26" />
9-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml
10
11    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
11-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:4:5-79
11-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:4:22-77
12    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
12-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:5:5-80
12-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:5:22-78
13    <uses-permission android:name="android.permission.CAMERA" />
13-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:6:5-64
13-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:6:22-62
14    <uses-permission android:name="android.permission.INTERNET" />
14-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:7:5-66
14-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:7:22-64
15
16    <application
16-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:9:5-28:19
17        android:allowBackup="true"
17-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:10:9-35
18        android:debuggable="true"
19        android:icon="@mipmap/ic_launcher"
19-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:11:9-43
20        android:label="@string/app_name"
20-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:12:9-41
21        android:roundIcon="@mipmap/ic_launcher_round"
21-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:13:9-54
22        android:supportsRtl="true"
22-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:14:9-35
23        android:theme="@style/AppTheme" >
23-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:15:9-40
24        <activity
24-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:16:9-27:20
25            android:name="com.via.viadetectdemo.Coco_Detection"
25-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:17:13-43
26            android:configChanges="orientation|keyboardHidden|screenSize"
26-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:18:13-74
27            android:label="@string/app_name"
27-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:19:13-45
28            android:screenOrientation="landscape"
28-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:21:13-50
29            android:theme="@style/FullscreenTheme" >
29-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:20:13-51
30            <intent-filter>
30-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:22:13-26:29
31                <action android:name="android.intent.action.MAIN" />
31-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:23:17-69
31-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:23:25-66
32
33                <category android:name="android.intent.category.LAUNCHER" />
33-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:25:17-77
33-->/home/jim/Desktop/2019MeichuHackathon/VIACocoDetector/viadetectdemo/src/main/AndroidManifest.xml:25:27-74
34            </intent-filter>
35        </activity>
36
37        <meta-data
37-->[com.android.support:appcompat-v7:26.1.0] /home/jim/.gradle/caches/transforms-2/files-2.1/c589bb7ec2d841c67ae68a3682166223/appcompat-v7-26.1.0/AndroidManifest.xml:26:9-28:38
38            android:name="android.support.VERSION"
38-->[com.android.support:appcompat-v7:26.1.0] /home/jim/.gradle/caches/transforms-2/files-2.1/c589bb7ec2d841c67ae68a3682166223/appcompat-v7-26.1.0/AndroidManifest.xml:27:13-51
39            android:value="26.1.0" />
39-->[com.android.support:appcompat-v7:26.1.0] /home/jim/.gradle/caches/transforms-2/files-2.1/c589bb7ec2d841c67ae68a3682166223/appcompat-v7-26.1.0/AndroidManifest.xml:28:13-35
40        <meta-data
40-->[android.arch.lifecycle:runtime:1.0.0] /home/jim/.gradle/caches/transforms-2/files-2.1/d0f6449185399ce77e6ff49656abaac3/runtime-1.0.0/AndroidManifest.xml:25:9-27:47
41            android:name="android.arch.lifecycle.VERSION"
41-->[android.arch.lifecycle:runtime:1.0.0] /home/jim/.gradle/caches/transforms-2/files-2.1/d0f6449185399ce77e6ff49656abaac3/runtime-1.0.0/AndroidManifest.xml:26:13-58
42            android:value="27.0.0-SNAPSHOT" />
42-->[android.arch.lifecycle:runtime:1.0.0] /home/jim/.gradle/caches/transforms-2/files-2.1/d0f6449185399ce77e6ff49656abaac3/runtime-1.0.0/AndroidManifest.xml:27:13-44
43    </application>
44
45</manifest>
