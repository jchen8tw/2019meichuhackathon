//package com.github.nkzawa.socketio.androidchat;
package com.via.viadetectdemo.Media;

import android.os.Bundle;
import android.os.Build;
import android.app.Application;
import io.socket.client.IO;
import io.socket.client.Socket;

import java.net.URISyntaxException;

public class ChatApplication {

    /*
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSocket.connect();
    }
     */
    private Socket mSocket;

    public ChatApplication(){
        try {
            mSocket = IO.socket("http://140.112.18.177:7122");
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket() {
        return mSocket;
    }

    public void connect(){
        mSocket.connect();
    }
}