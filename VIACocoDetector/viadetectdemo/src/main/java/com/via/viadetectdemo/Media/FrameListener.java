package com.via.viadetectdemo.Media;

import android.media.Image;

/**
 * Created by HankWu on 2018/8/9.
 */

public interface FrameListener {
    void onImageAvailable(Image image);
}
