/*
 * Copyright Notice
 * Copyright © 2019 VIA Technologies Inc. All Rights Reserved. No part of this document may be
 * reproduced, transmitted, transcribed, stored in a retrieval system, or translated into any language, in
 * any form or by any means, electronic, mechanical, magnetic, optical, chemical, manual or otherwise
 * without the prior written permission of VIA Technologies Inc. The material in this document is for
 * information only and is subject to change without notice. VIA Technologies Inc. reserves the right to
 * make changes in the product design without reservation and without notice to its users.
 *
 * Trademarks
 * A920 and AltaDs3 may only be used to identify products of VIA Technologies, Inc.
 * All trademarks are the properties of their prospective owners.
 */

package com.via.viadetectdemo;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumented test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.via.viadetectdemo", appContext.getPackageName());
    }
}
