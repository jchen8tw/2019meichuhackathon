/*
 * Copyright Notice
 * Copyright © 2019 VIA Technologies Inc. All Rights Reserved. No part of this document may be
 * reproduced, transmitted, transcribed, stored in a retrieval system, or translated into any language, in
 * any form or by any means, electronic, mechanical, magnetic, optical, chemical, manual or otherwise
 * without the prior written permission of VIA Technologies Inc. The material in this document is for
 * information only and is subject to change without notice. VIA Technologies Inc. reserves the right to
 * make changes in the product design without reservation and without notice to its users.
 *
 * Trademarks
 * A920 and AltaDs3 may only be used to identify products of VIA Technologies, Inc.
 * All trademarks are the properties of their prospective owners.
 */

package com.via.viadetectdemo;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() throws Exception {
        assertEquals(4, 2 + 2);
    }
}